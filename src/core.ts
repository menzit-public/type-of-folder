import fs from 'node:fs/promises'


export type FileTransformationOption = 'removeExtension'
// export type FileTransformationOption = 'removeExtension' | 'toCamelCase'|'toKebabCase'|'removeNumbers'|'removeNonNumbersAndWords'|'removeWords'
export type TofOptions = {
  inputFolder: string
  outputFile: string
  typeName: string
  includeRegex?: RegExp
  excludeRegex?: RegExp
  fileTransformations?: Array<FileTransformationOption>
  shouldExportDefault?: boolean
  shouldWatch?: boolean
  shouldAppendFile?: boolean
  shouldBeSilent?: boolean
}

export const typeOfFolder = async (options: TofOptions) => {
  const inputFolder = options.inputFolder
  const outputFile = options.outputFile
  const typeName = options.typeName
  const includeRegex = options.includeRegex
  const excludeRegex = options.excludeRegex
  const shouldExportDefault = !!options.shouldExportDefault
  const shouldWatch = !!options.shouldWatch
  const shouldBeSilent = !!options.shouldBeSilent
  let shouldAppendFile = options.shouldAppendFile !== false
  const fileTransformations = options.fileTransformations || ['removeExtension']

  if (!inputFolder) throw Error('No input folder given')
  if (!outputFile) throw Error('No output file given')
  if (!typeName) throw Error('No name for type given')

  // check files
  const fileNames = await fs.readdir(inputFolder)
  if (fileNames.length === 0) fileNames.push('<no files found>')

// make new content
  let newFileContent = `export${shouldExportDefault ? ' default ': ' '}type ${typeName} =`
  newFileContent += `${fileNames.filter(filename => {
    if (excludeRegex && excludeRegex.test(filename)) return false
    if (includeRegex && !includeRegex.test(filename)) return false
    return filename
  }).map(filename => {
    return `'${transformFilename(filename, fileTransformations)}'`
  }).join('|')}\n`
// check shouldAppendFile
  shouldAppendFile = shouldAppendFile && (await fs.stat(outputFile).catch(() => ({isFile: () => false}))).isFile()

  if (shouldAppendFile) {
    const file = await fs.readFile(outputFile) // file is always there because we check beforehand in appendFile flag
    const fileContent = file.toString()
    // https://regex101.com/r/qqKKBE/1
    // every `\` requires extra `\` in string so `\\`
    const regex = new RegExp(`(export\\s(default)?\\s?type\\sFlatIcon\\s=\\n?\\s*)(\\s*\\|?\\s*(['"].*['"])\\s*\\|?\\s*)+;?`, 'g');
    const match = regex.exec(fileContent)

    // replace, else append
    if (match && match.length) {
      // if not end of file, add extra newline to put newline before next line of code
      if (regex.lastIndex < fileContent.length) {
        newFileContent += '\n'
      }
      newFileContent = fileContent.replace(regex, newFileContent)
      await fs.writeFile(outputFile, newFileContent)
    } else {
      await fs.appendFile(outputFile, newFileContent)
    }
  } else {
    // overwrite
    await fs.writeFile(outputFile, newFileContent)
  }

  if (shouldWatch) {
    const chokidar = require('chokidar')
    const watcher = chokidar.watch(inputFolder, {
      ignored: '',
      ignoreInitial: true,
    });
    watcher
      .on('add', async (path: string) => {
        shouldBeSilent || console.log(`File ${path} has been added, rerunning 'typeOfFolder'`)
        await typeOfFolder({...options, shouldWatch: false})
      })
      .on('change', async (path: any) => {
        shouldBeSilent || console.log(`File ${path} has been changed, rerunning 'typeOfFolder'`)
        await typeOfFolder({...options, shouldWatch: false})
      })
      .on('unlink', async (path: string) => {
        shouldBeSilent || console.log(`File ${path} has been removed, rerunning 'typeOfFolder'`)
        await typeOfFolder({...options, shouldWatch: false})
      });
  }
  return newFileContent
}


const transformFilename = (subject: string, transformations: FileTransformationOption[]) => {
  // https://regex101.com/r/0JxJ74/1/
  const extensionRegex = /(?<extension>\.\w*)*$/
  const {extension} = extensionRegex.exec(subject)?.groups as {extension:string}
  for (const transformation of transformations) {
    switch (transformation) {
      case 'removeExtension':
        subject = subject.replace(extensionRegex, '')
        break
      default:
        console.error(`IMPLEMENT TRANSFORMATION :: ${transformation}`)
    }
  }
  return subject
}
