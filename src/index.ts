import {FileTransformationOption, TofOptions, typeOfFolder} from "../src/core"

export default typeOfFolder
export {FileTransformationOption, TofOptions}
