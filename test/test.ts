import {FileTransformationOption, TofOptions, typeOfFolder} from "../src/core"

const inputFolder = '.'
const outputFile = './FlatIcons.ts'
const typeName = 'FlatIcon'
const shouldExportDefault = false
const shouldBeSilent = false
const shouldWatch = true
let shouldAppendFile = true
const fileTransformations: FileTransformationOption[] = []

typeOfFolder({
  inputFolder,
  outputFile,
  typeName,
  shouldExportDefault,
  shouldAppendFile,
  shouldBeSilent,
  shouldWatch,
  fileTransformations,
})

export default typeOfFolder
export {FileTransformationOption, TofOptions}
